# Documentation
* [Invoice API](#invoice-api)

## Invoice API
### GET /invoices
Returns all Invoice instance.

Example response:
```json
[
    {
        "invoicePrice": 250,
        "_id": "5dd279e270bead896a12b181",
        "invoiceItems": [
            {
                "price": 200,
                "_id": "5dd279e270bead896a12b183",
                "title": "Test1"
            },
            {
                "price": 50,
                "_id": "5dd279e270bead896a12b182",
                "title": "Test2"
            }
        ],
        "created_at": "2019-11-18T11:00:50.005Z",
        "__v": 0
    }
]
```

### POST /invoices
Adds to Invoice instance in a database.

|Param|Type|
|--|--|
|invoiceItems|`array`|
|invoicePrice|`number`|

Example request:
```json
{
    "invoicePrice": 250,
    "invoiceItems": [
        {
            "price": 200,
            "_id": "5dd279e270bead896a12b183",
            "title": "Test1"
        },
        {
            "price": 50,
            "_id": "5dd279e270bead896a12b182",
            "title": "Test2"
        }
    ]
}
```
Example response:
```json
{
    "message": "Successfully created!"
}
```

### GET /invoices/:id
Returns a Invoice instance by id.

Example response:
```json
{
    "invoicePrice": 250,
    "_id": "5dd279e270bead896a12b181",
    "invoiceItems": [
        {
            "price": 200,
            "_id": "5dd279e270bead896a12b183",
            "title": "Test1"
        },
        {
            "price": 50,
            "_id": "5dd279e270bead896a12b182",
            "title": "Test2"
        }
    ],
    "created_at": "2019-11-18T11:00:50.005Z",
    "__v": 0
}
```

### DELETE /invoices/:id
Delete a Invoice instance by id.

Example response:
```json
{
    "message": "Successfully deleted!"
}
```
